# CS205 Final Project: Chess in Java

John Richardson, Andrew Bacher, Garret Landon, Eadoin Murphy

##Instructions

In order to play our fully functional chess game, simply run the java file "ChessGUI". 
If the user is unfamiliar with the rules of chess, please look at the following link: https://www.chesscoachonline.com/chess-articles/chess-rules. Once the program is run, white is played by the user and makes the first move. To move a piece, the user clicks on a piece and available valid moves are highlighted in green. 
Once moved, the computer will choose a random valid to make. 
This process continues until either the computer or user places one another in checkmate. 
The previous moves made can be seen on the panel on the left side of the board. 
Furthermore, pieces that the user has taken from the computer can be seen at the bottom of the screen, and subsequently the pieces that the computer takes from the user can be seen on the right. A
t the top of the screen, there is a new game button if the user should choose to restart the game. 
Points earned from either side can be seen next to the new game button. 


